using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Windows;
using System.IO.Ports;
using System.Threading;
using System.Diagnostics;

namespace wpf_parseissue
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        // Setup TaskFactory
        private readonly TaskFactory uiTaskFactory;
        private readonly TaskScheduler uiScheduler;

        // Setup thread lock
        private object thisLock = new object();

        // Initialize a new instance of the com port
        SerialPort com = new SerialPort();

        // Initialize a stopwatch for timeouts
        Stopwatch sw = new Stopwatch();


        // This is where I was previously doing it. However, var won't work here. 
        // I believe this is because of var requiring to be in a local variable.
        // Info here: https://docs.microsoft.com/en-us/dotnet/csharp/misc/cs0825
        //byte[] tmpBuffer = new byte[0];
        //byte[] chunked_message = new byte[0];
        //byte[] header_message = new byte[0];   // Header is always 16 bytes
        //byte[] data_message = new byte[0];
        //byte[] full_message = new byte[0];

        public MainWindow()
        {
            InitializeComponent();
            // Construct a task scheduler from the current SynchronizationContext (UI thread)
            uiScheduler = TaskScheduler.FromCurrentSynchronizationContext();
            // Construct a new TaskFactory using our UI scheduler
            uiTaskFactory = new TaskFactory(uiScheduler);
            
            tmp_buffer = new List<byte>();
            header_message = new List<byte>();
            full_message = new List<byte>();
        }
        
        
        // Outer Variables
        string ComPort = "";
        string indata = "";
        bool com_changed = false;
        List<byte> tmp_buffer;
        List<byte> header_message;
        List<byte> full_message;

        // Handler that handles all com port data inputs
        // System can "Send" once and multiple events be triggered. Be sure to check to see if everythign is getting added to buffer
        private void DataReceivedHandler(object sender, SerialDataReceivedEventArgs e)
        {
            SerialPort sp = (SerialPort)sender;
            // if changing com port, read into indata for exiting instead of calculating bytes
            if (com_changed == false)
            {
                indata = sp.ReadExisting();
            }
            else
            {
                int numberBytes = sp.BytesToRead;
                byte[] buffer = new byte[numberBytes];
                sp.Read(buffer, 0, numberBytes);
                MessageManipulation(buffer);
            }
        }

        private void MessageManipulation(byte[] buffer)
        {
            // Lists for message manipulation

            lock (thisLock)
            {
                // Add buffer to list for manipulation
                tmp_buffer.AddRange(buffer);

                // Check to make sure the buffer 
                if (tmp_buffer.Count > 20)
                {

                }
            }

            // Previous version
            /* byte[] headerMessage = buffer.Skip(2).Take(16).ToArray();
               tmpBuffer = tmpBuffer.Concat(buffer).ToArray();
               if (tmpBuffer.Length > 20)
               {
                    // Copy Packet Header to new variable
                    header_message = header_message.Concat(tmpBuffer.SkipWhile(start => start != 85).Take(20)).ToArray();

                    // Find out how much data is left to get (byte 14 of the header including 0)
                    int payload_length = header_message[14];

                    // Check for full message
                    if (tmpBuffer.Length >= 20 + payload_length + 1) // make sure to account for the A9 at the end
                    {
                        // Go ahead and get full message
                        full_message = full_message.Concat(tmpBuffer.SkipWhile(start => start != 85).Take(20 + payload_length + 1)).ToArray();
                        // If last byte is not end of packet, exit 
                        if(full_message[full_message.Length-1] != 169)
                        {
                            return;
                        }
                    }
                    else // clear buffer if message sections are missing
                    {
                        //Array.Clear(tmpBuffer, 0, tmpBuffer.Length - 1);
                        return; // Leave subfunction since bad packet received
                    }
                }
             */
        }

    }
}
